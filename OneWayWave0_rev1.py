##############################################################
# Initial tests - Piotr Darnowski, 23-07-2017, Revision 2
# Problem 0. One-way wave equation (Sommerfeld Wave Equation or Advection equation) u_t = c*u_x  c - constant speed (1 hyperbolic PDE) 1st order, linear, constant coefficients.
# analitical solution: u(x,t) = u(x+ct,0)  for any function u
# Benchmark case - travelling 1D square or sine wave (signal) with velocity c:
# for example:  u(x,0) =  0  for x<0 and x>1 and  1.0 for  0<x<1
# Solution methods (U - is numerical approximation of u):
# 1) Forward FD (Upwind), low accuracy (order p=1), stable for 0<=r<=1
# 			(U(x,t+dt) - U(x,t))/dt = (c/dx)*(U(x+dx,t)-U(x,t))
#   more useful:       U(x,t+dt)  = (1-r)*U(x,t) + r*U(x+dx,t)
# 2) Lax-Friedrichs FD, low accuracy, stable for  -1<=r<=1
#		 (U(x,t+dt) -  0.5*(U(x+dx,t) + U(x-dx,t)) )/dt   = (c/2dx)* (U(x+dx,t) - U(x-dx,t))
#      			U(x,t+dt) =  2*r* (U(x+dx,t) - U(x-dx,t))  +  0.5*(U(x+dx,t) + U(x-dx,t))
#					U(x,t+dt) =   (2*r + 0.5)*U(x+dx,t) 	+ (0.5 - 2*r)*	U(x-dx,t)
# 3) Lax-Wendroff FD, extra accruacy, stable for  -1<=r<=1
# 			(U(x,t+dt) - U(x,t))/dt  =  (c/(2*dx))*(U(x+dx,t) - U(x-dx,t))   + (dt*c^2/(2*dx^2))* (  U(x+dx,t) - 2*U(x,t) + U(x-dx,t) )
#  more useful:		U(x,t+dt) = (1-r^2)*U(x,t) + (1/2) *(r^2+r)*U(x+dx,t) + (1/2)*(r^2-r)*U(x-dx,t))    
# Notes:
# Courant number r = c*dt/dx - crucial for stability
# CFL condition: r <= 1.0
# All based on Chapter 6: G. Strang, Computational Science and Enginnering
##############################################################
# LIBRARIES
import numpy as np          # Make all matlib functions accessible at the top level via M.func()
import matplotlib as mpl
import matplotlib.pyplot as plt
from numpy import pi
from numpy import sin
#from scipy import linalg

# Signal  definition is here
def signal1( position):		#Square signal u(x,0) = 1.0 for  7<=x<=8.5
	if  (position>=0.7) and (position<=0.85): 
		f = 1.0
	else:	
		f = 0.0
	return f;
def signal2( position):			#Sine signal u(x,0) = 1.0 for  7<=x<=8.5
	if  (position>=0.7) and (position<=0.85): 
		f = sin(2*pi*(position-0.7)/0.3)
	else:	
		f = 0.0
	return f;	
	
	
# Main Code
# t = n*dt    x = j*dx   - grid 
L = 1.0 			#box length
N = 201  			#nodes
dx = L/(N-1)  	#spatial step	
c = 2.0  			#wave speed  
dt = 0.0001   # CFL condition dt <= dx/c = (1/20)/1 = 0.05;
r = c*dt/dx
print(r)
T = 500 #number of time steps to be computed
t_end = T*dt
x 		=  np.arange(0,L+dx,dx) #computational space domain
U 		= np.zeros((N,T))  #solution matrix initialization
ULW = np.zeros((N,T))  #solution matrix initialization
ULF  = np.zeros((N,T))  #solution matrix initialization
u 		= np.zeros((N,T))  #solution matrix initialization - analitical solution

# Analitical solution
for n in range(0,T):
	for j in range(0,N): 
		u[j,n] = signal1(x[j]+c*n*dt) #square signal movment
		#u[j,n] = signal2(x[j]+c*n*dt) #square signal movment
		
# Upwind solution
# U(x,t+dt)  = (1-r)*U(x,t) + r*U(x+dx,t)
# LAX-WENDROFF
# U(x,t+dt) = (1-r^2)*U(x,t) + (1/2) *(r^2+r)*U(x+dx,t) + (1/2)*(r^2-r)*U(x-dx,t))    
# LAX-FRIEDRICH
# U(x,t+dt) =   (2*r + 0.5)*U(x+dx,t) 	+ (0.5 - 2*r)*	U(x-dx,t)

#Initial condition U(x,0)
for j in range(0,N): 
		U[j,0] = signal1(x[j])  #initial square signal
		ULW[j,0] = signal1(x[j])  #initial square signal
		ULF[j,0] = signal1(x[j])  #initial square signal
	#	U[j,0] = signal2(x[j])  #initial square signal
	#	ULW[j,0] = signal2(x[j])  #initial square signal
	#	ULF[j,0] = signal2(x[j])  #initial square signal
# Solution U(x,t)		
for n in range(0,T-1):			#time loop	
	for j in range(0,N-2):
		U[j,n+1] 	   = (1.0-r)*U[j,n] + r*U[j+1,n] 
		
for n in range(0,T-1):			#time loop	
	for j in range(1,N-2):		
		ULW[j,n+1] = (1.0-r*r)*ULW[j,n] + (0.5)*(r*r+r)*ULW[j+1,n] + 0.5*(r*r-r)*ULW[j-1,n]    
		
for n in range(0,T-1):			#time loop	
	for j in range(1,N-2):		
		ULF[j,n+1] = (2.0*r+0.5)*U[j+1,n] + (0.5-2.0*r)*U[j-1,n] 

time = 0
plt.plot(x,u[:,time],x,U[:,time],'o',x,ULW[:,time],'--',x,ULF[:,time],'-.')	
plt.legend(['Exact','Upwind','Lax-Wendroff','Lax-Friedrich'])
plt.title('0 time steps')
plt.grid(True)
plt.savefig('fig_1.eps', format = 'eps')
plt.close()

time = int(np.floor(T/2))
plt.plot(x,u[:,time],x,U[:,time],'-o',x,ULW[:,time],'--',x,ULF[:,time],'-.')		
plt.legend(['Exact','Upwind','Lax-Wendroff','Lax-Friedrich'])	
plt.title('~T/2 time steps')
plt.savefig('fig_2.png')
plt.close()
time = T-1
plt.plot(x,u[:,time],x,U[:,time],'-o',x,ULW[:,time],'--',x,ULF[:,time],'-.')			
plt.legend(['Exact','Upwind','Lax-Wendroff','Lax-Friedrich'])
plt.title('~T time steps')
plt.savefig('fig_3.png')
plt.close()

plt.plot(x,U[:,0],x,U[:,100],x,U[:,200],x,U[:,300],x,U[:,400])
plt.legend(['0 steps', '100', '200','300','400','499'])
plt.title('Upwind time evolution')
plt.savefig('fig_4.png')
plt.close()

eU = (U[:,time] - u[:,time])
eULW = (ULW[:,time]-u[:,time])
eULF = (ULF[:,time] - u[:,time])

# Absolute Error comparison for the time step T-1
plt.plot(x,eU,'r-',x,eULW,'--b',x,eULF,'-.g')
plt.legend(['Upwind','Lax-Wendroff','Lax-Friedrich'])
plt.title('Absolute Error')
plt.savefig('fig_5.png')
plt.close()







